# My project's README


# Homework 01, 2018.01.29:  9/10 #

Nice job! 

### Kudos ###

* Nice job using a stackLayout to organize multiple elements.
* Good work on the styling.
* Excellent job using UI customizations we have not even mentioned in class!
* Great work with the functionality implemented from the code-behind... Loved it!

### Opportunities for improvement ###

* Just remember to submit future assignments on time. Late assignemnts will receive feedback, but not credit.