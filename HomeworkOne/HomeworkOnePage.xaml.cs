﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HomeworkOne
{
    public partial class HomeworkOnePage : ContentPage
    {
        static int count = 1;

        public HomeworkOnePage()
        {
            InitializeComponent(); 

            TitleLabel.TextColor = Color.Aqua;
            TitleLabel.FontSize = 30;

            ButtonNext.Text = "Next";
            ButtonNext.TextColor = Color.White;
            ButtonNext.FontSize = 25;
        }

        void Handle_Clicked(object sender, System.EventArgs e)
        {
            switch (count)
            {
                case 0:
                    ContentLabel.Text = "";
                    TitleLabel.FontSize = 30;
                    ButtonNext.Text = "Next";
                    break;
                case 1:
                    ContentLabel.Text = "My name is Geraldine";
                    ContentLabel.FontSize = 25;
                    TitleLabel.FontSize = 20;
                    break;
                case 2:
                    ContentLabel.Text = "I'm an international student from France";
                    break;
                case 3:
                    ContentLabel.Text = "My major is computer sciences";
                    ButtonNext.Text = "Restart";
                    count = -1;
                    break;
                default:
                    break;
            }
            count++;
        }
    }
}
